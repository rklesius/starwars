"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var People = /** @class */ (function () {
    function People(name, height, mass, hair_color, skin_color, birth_year, gender, homeworld, films, species, vehicles, starships, created, edited) {
        this.name = name;
        this.height = height;
        this.mass = mass;
        this.hair_color = hair_color;
        this.skin_color = skin_color;
        this.birth_year = birth_year;
        this.gender = gender;
        this.homeworld = homeworld;
        this.films = films;
        this.species = species;
        this.vehicles = vehicles;
        this.starships = starships;
        this.created = created;
        this.edited = edited;
    }
    return People;
}());
exports.People = People;
var PeopleService = /** @class */ (function () {
    function PeopleService() {
    }
    
    return PeopleService;
}());
exports.PeopleService = PeopleService;