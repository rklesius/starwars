import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { PeopleService, People } from './people-service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  id : number; 
  people : People;

  constructor(private httpClient:HttpClient, private route: ActivatedRoute, private ps : PeopleService) {
    this.id = this.route.snapshot.params['id'];  
    ps.getPeople(this.id).subscribe(response => {
      this.people = response;
    })
  }

  ngOnInit() {
  }

}
