import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export class People {
    constructor(
        public name: string,
        public height: string, 
        public mass: string,
        public hair_color: string,
        public skin_color: string,
        public birth_year: string,
        public gender: string, 
        public homeworld: string,
        public films: Array<string>,
        public species: Array<string>,
        public vehicles: Array<string>,
        public starships: Array<string>,
        public created: string,
        public edited: string,
    ){}
}

@Injectable()
export class PeopleService {
    constructor(private httpClient:HttpClient) {}

    getPeople(id:number): Observable<People> {
        return this.httpClient.get<People>('https://swapi.co/api/people'+id);
    }
}