import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { PeopleComponent } from './people/people.component';
import { PlanetsComponent } from './planets/planets.component';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { SpeciesComponent } from './species/species.component';
import { StarshipsComponent } from './starships/starships.component';


const routes: Routes = [
  {path: 'people/:id', component: PeopleComponent},
  {path: 'planets/:id', component: PlanetsComponent},
  {path: 'vehicles/:id', component: VehiclesComponent},
  {path: 'species/:id', component: SpeciesComponent},
  {path: 'starships/:id', component: StarshipsComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
