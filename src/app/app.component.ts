import { Component } from '@angular/core';
import { FormGroup } from '@angular/Forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'StarWars';
  categories: Array<string> = ["people", "planets", "vehicles", "species", "starships"];
  
  selectedCategory: string;
  selectedItem: number;

  constructor(private router: Router) {}

  showItem(f : FormGroup) {
    this.router.navigate(['/'+ this.selectedCategory + '/' + this.selectedItem]);
  }

}
